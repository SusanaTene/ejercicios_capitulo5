print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 2 del capitulo 5. Autora: Lilia Susana Tene')

#Escribe otro programa que pida una lista de números como
#la anterior y al final muestre por pantalla el máximo y mínimo de los
#números, en vez de la media.
cuantos=int(input("Cuantos numeros deseas ingresar?\n"))
numero = float(input("Escriba el número 1: "))
minimo = maximo = numero
for i in range(2,cuantos+1):
    print("Ingrese el numero", i,":")
    numero=float(input())
    if(numero>maximo):
        maximo=numero
    elif(numero<minimo):
        minimo=numero
print("El valor maximo es el numero",maximo)
print("El valor minimo es el numero",minimo)