print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 1 del capitulo 5. Autora: Lilia Susana Tene')

#Ejercicio 1: Escribe un programa que lea repetidamente números hasta que el usuario introduzca
#“fin”. Una vez se haya introducido “fin”, muestra por pantalla el total, la cantidad de números
#y la media de esos números. Si el usuario introduce cualquier otra cosa que no sea un número,
#detecta su fallo usando try y except, muestra un mensaje de error y pasa al número siguiente.

contador = 0
total = 0

while True:
    valor = input("Introduce un número (o 'fin' para terminar): ")
    if valor == 'fin':
        break
    try:
        total = int(valor) + total
        contador = contador + 1
        media = total/contador
    except ValueError:
        print("Entrada Invalida. Intenta de nuevo...")

print("La suma de los números es: ", total)
print("La cantidad de números introducidos es: ", contador)
print("La media de los valores es:  ", media)

